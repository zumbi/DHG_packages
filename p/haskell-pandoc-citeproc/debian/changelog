haskell-pandoc-citeproc (0.14.3.1-2) UNRELEASED; urgency=medium

  * Set Rules-Requires-Root to no.

 -- Clint Adams <clint@debian.org>  Sun, 06 May 2018 22:10:16 -0400

haskell-pandoc-citeproc (0.14.3.1-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sun, 29 Apr 2018 18:51:23 -0400

haskell-pandoc-citeproc (0.14.3-1) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.
  * New upstream release

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 16:43:39 -0400

haskell-pandoc-citeproc (0.10.5.1-1) unstable; urgency=medium

  [ Sean Whitton ]
  * New upstream release

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 05 Nov 2017 13:25:19 +0200

haskell-pandoc-citeproc (0.10.4.1-2) unstable; urgency=medium

  * Team upload
  * Depend on libghc-aeson-pretty-dev to fix a build failure.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sat, 24 Jun 2017 01:38:00 +0200

haskell-pandoc-citeproc (0.10.4.1-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sat, 17 Jun 2017 15:14:31 -0400

haskell-pandoc-citeproc (0.10.2.2-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Mon, 31 Oct 2016 21:03:43 -0400

haskell-pandoc-citeproc (0.10.1.2-2) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:35:19 -0400

haskell-pandoc-citeproc (0.10.1.2-1) experimental; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Wed, 26 Oct 2016 19:09:29 -0400

haskell-pandoc-citeproc (0.10.1.1-1) experimental; urgency=medium

  * New upstream version.

 -- Clint Adams <clint@debian.org>  Fri, 07 Oct 2016 21:40:11 -0400

haskell-pandoc-citeproc (0.9.1.1-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Ilias Tsitsimpis ]
  * New upstream release

 -- Ilias Tsitsimpis <i.tsitsimpis@gmail.com>  Mon, 30 May 2016 11:16:54 +0000

haskell-pandoc-citeproc (0.9-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 22 Feb 2016 13:03:29 +0100

haskell-pandoc-citeproc (0.8.1.3-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sun, 10 Jan 2016 23:15:17 -0500

haskell-pandoc-citeproc (0.7.4-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Fri, 04 Dec 2015 00:18:54 -0500

haskell-pandoc-citeproc (0.7.3-3) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:52 -0500

haskell-pandoc-citeproc (0.7.3-2) experimental; urgency=medium

  * Add lintian override for spurious rpaths

 -- Joachim Breitner <nomeata@debian.org>  Fri, 21 Aug 2015 09:13:59 +0200

haskell-pandoc-citeproc (0.7.3-1) experimental; urgency=medium

  * New upstream release
  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:28:22 +0200

haskell-pandoc-citeproc (0.6.0.1-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Wed, 29 Apr 2015 13:25:58 +0200

haskell-pandoc-citeproc (0.4.0.1-2) experimental; urgency=medium

  * Depend on haskell-devscripts 0.9, found in experimental

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 17:11:37 +0100

haskell-pandoc-citeproc (0.4.0.1-1) unstable; urgency=medium

  * New upstream release

 -- Raúl Benencia <rul@kalgan.cc>  Tue, 30 Sep 2014 12:52:26 -0300

haskell-pandoc-citeproc (0.3.0.1-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sat, 26 Apr 2014 12:53:09 +0200

haskell-pandoc-citeproc (0.2-3) unstable; urgency=medium

  * Fix data file handling (closes: #734155):
    - Install chicago-author-date.csl in libghc-pandoc-citeproc-data.
    - Make libghc-pandoc-citeproc-dev and pandoc-citeproc depend on
      libghc-pandoc-citeproc-data.
    - Fix paths of locale files to match what Text.CSL.Data expects.
  * Set LANG when running the test suite (though to en_US.UTF-8, not C.UTF-8
    which doesn't work) as well as LC_ALL.

 -- Colin Watson <cjwatson@debian.org>  Wed, 08 Jan 2014 12:15:42 +0000

haskell-pandoc-citeproc (0.2-2) unstable; urgency=medium

  * Run tests under C.UTF-8.

 -- Clint Adams <clint@debian.org>  Fri, 03 Jan 2014 15:26:44 -0500

haskell-pandoc-citeproc (0.2-1) unstable; urgency=medium

  * Initial release.

 -- Clint Adams <clint@debian.org>  Wed, 01 Jan 2014 19:20:42 -0500
