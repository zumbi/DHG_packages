Source: haskell-hoogle
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Kiwamu Okabe <kiwamu@debian.or.jp>,
 Iustin Pop <iustin@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends: debhelper (>= 10),
 haskell-devscripts-minimal | haskell-devscripts (>= 0.9),
 cdbs,
 ghc,
 ghc-prof,
 libghc-quickcheck2-dev,
 libghc-quickcheck2-prof,
 libghc-aeson-dev,
 libghc-aeson-prof,
 libghc-cmdargs-dev,
 libghc-cmdargs-prof,
 libghc-conduit-dev (>= 1.3.0),
 libghc-conduit-prof,
 libghc-conduit-extra-dev (>= 1.2.3.2),
 libghc-conduit-extra-prof,
 libghc-connection-dev,
 libghc-connection-prof,
 libghc-extra-dev (>= 1.6.6),
 libghc-extra-prof,
 libghc-src-exts-dev (>= 1.18),
 libghc-src-exts-dev (<< 1.21),
 libghc-src-exts-prof,
 libghc-http-conduit-dev (>= 2.3),
 libghc-http-conduit-prof,
 libghc-http-types-dev,
 libghc-http-types-prof,
 libghc-js-flot-dev,
 libghc-js-flot-prof,
 libghc-js-jquery-dev,
 libghc-js-jquery-prof,
 libghc-mmap-dev,
 libghc-mmap-prof,
 libghc-network-dev (>= 2.6),
 libghc-network-prof,
 libghc-network-uri-dev (>= 2.6),
 libghc-network-uri-prof,
 libghc-old-locale-dev,
 libghc-old-locale-prof,
 libghc-process-extras-dev,
 libghc-process-extras-prof,
 libghc-resourcet-dev,
 libghc-resourcet-prof,
 libghc-storable-tuple-dev,
 libghc-storable-tuple-prof,
 libghc-tar-dev,
 libghc-tar-prof,
 libghc-text-dev,
 libghc-text-prof,
 libghc-uniplate-dev,
 libghc-uniplate-prof,
 libghc-utf8-string-dev,
 libghc-utf8-string-prof,
 libghc-vector-dev,
 libghc-vector-prof,
 libghc-wai-dev,
 libghc-wai-prof,
 libghc-wai-logger-dev,
 libghc-wai-logger-prof,
 libghc-warp-dev,
 libghc-warp-prof,
 libghc-warp-tls-dev,
 libghc-warp-tls-prof,
 libghc-zlib-dev,
 libghc-zlib-prof,
Build-Depends-Indep: ghc-doc,
 libghc-quickcheck2-doc,
 libghc-aeson-doc,
 libghc-cmdargs-doc,
 libghc-conduit-doc,
 libghc-conduit-extra-doc,
 libghc-connection-doc,
 libghc-extra-doc,
 libghc-src-exts-doc,
 libghc-http-conduit-doc,
 libghc-http-types-doc,
 libghc-js-flot-doc,
 libghc-js-jquery-doc,
 libghc-mmap-doc,
 libghc-network-doc,
 libghc-network-uri-doc,
 libghc-old-locale-doc,
 libghc-process-extras-doc,
 libghc-resourcet-doc,
 libghc-storable-tuple-doc,
 libghc-tar-doc,
 libghc-text-doc,
 libghc-uniplate-doc,
 libghc-utf8-string-doc,
 libghc-vector-doc,
 libghc-wai-doc,
 libghc-wai-logger-doc,
 libghc-warp-doc,
 libghc-warp-tls-doc,
 libghc-zlib-doc,
Standards-Version: 4.1.4
Homepage: http://hoogle.haskell.org/
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-hoogle
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-hoogle]

Package: libghc-hoogle-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Provides: ${haskell:Provides},
Description: Haskell API Search
 Hoogle is a Haskell API search engine, which allows you to
 search many standard Haskell libraries by either function name,
 or by approximate type signature.
 .
 This package contains the normal library files.

Package: libghc-hoogle-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Provides: ${haskell:Provides},
Description: Haskell API Search; profiling libraries
 Hoogle is a Haskell API search engine, which allows you to
 search many standard Haskell libraries by either function name,
 or by approximate type signature.
 .
 This package contains the libraries compiled with profiling enabled.

Package: libghc-hoogle-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Description: Haskell API Search; documentation
 Hoogle is a Haskell API search engine, which allows you to
 search many standard Haskell libraries by either function name,
 or by approximate type signature.
 .
 This package contains the documentation files.

Package: hoogle
Architecture: any
Section: misc
Depends: ghc-doc,
 libjs-jquery,
 libjs-chosen (>= 0.9.15),
 libjs-jquery-cookie,
 libghc-js-jquery-data,
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Description: Haskell API Search for Debian system
 Hoogle is a Haskell API search engine, which allows you to
 search many standard Haskell libraries by either function name,
 or by approximate type signature.
 .
 This package contains the hoogle command.
