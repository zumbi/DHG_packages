haskell-maths (0.4.8-6) UNRELEASED; urgency=medium

  * Set Rules-Requires-Root to no.

 -- Clint Adams <clint@debian.org>  Sun, 06 May 2018 22:10:06 -0400

haskell-maths (0.4.8-5) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:04:56 -0400

haskell-maths (0.4.8-4) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:34:50 -0400

haskell-maths (0.4.8-3) experimental; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Clint Adams ]
  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Fri, 14 Oct 2016 16:42:00 -0400

haskell-maths (0.4.8-2) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:45 -0500

haskell-maths (0.4.8-1) experimental; urgency=medium

  * New upstream release
  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:28:08 +0200

haskell-maths (0.4.5-3) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:51:29 +0200

haskell-maths (0.4.5-2) experimental; urgency=low

  * Adjust watch file to new hackage layout
  * Depend on haskell-devscripts 0.9, found in experimental

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 17:11:20 +0100

haskell-maths (0.4.5-1) unstable; urgency=low

  * New upstream release.

 -- Iulian Udrea <iulian@ubuntu.com>  Fri, 28 Jun 2013 20:55:07 +0100

haskell-maths (0.4.3-3) unstable; urgency=low

  * Enable compat level 9
  * Use substvars for Haskell description blurbs

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:51:22 +0200

haskell-maths (0.4.3-2) experimental; urgency=low

  * Depend on haskell-devscripts 0.8.13 to ensure this packages is built
    against experimental
  * Bump standards version, no change

 -- Joachim Breitner <nomeata@debian.org>  Sun, 14 Oct 2012 12:01:29 +0200

haskell-maths (0.4.3-1) unstable; urgency=low

  * New upstream release.
  * Drop the add-Eq-constraints patch; applied upstream.
  * Update debian/copyright.

 -- Iulian Udrea <iulian@physics.org>  Thu, 16 Feb 2012 18:55:59 +0000

haskell-maths (0.4.1-2) unstable; urgency=low

  * Depend on libghc-random-dev, as it was split out from GHC
  * patches/add-Eq-Constraints: Add Eq constraints where necessary

 -- Joachim Breitner <nomeata@debian.org>  Sat, 04 Feb 2012 12:17:46 +0100

haskell-maths (0.4.1-1) unstable; urgency=low

  * New upstream release.
    New modules:
    - Math.Algebras.Octonions
    - Math.NumberTheory.Prime
    - Math.NumberTheory.Factor

 -- Iulian Udrea <iulian@linux.com>  Thu, 24 Nov 2011 18:28:32 +0000

haskell-maths (0.4.0-1) unstable; urgency=low

  * New upstream release.
  * Drop libghc-quickcheck2-{dev,prof,doc}.
  * Update copyright year.

 -- Iulian Udrea <iulian@linux.com>  Sat, 24 Sep 2011 15:16:33 +0100

haskell-maths (0.3.4-1) unstable; urgency=low

  * New upstream release.

 -- Iulian Udrea <iulian@linux.com>  Sat, 10 Sep 2011 18:47:29 +0100

haskell-maths (0.3.3-1) unstable; urgency=low

  * Initial release.

 -- Iulian Udrea <iulian@linux.com>  Fri, 12 Aug 2011 07:04:34 +0100
