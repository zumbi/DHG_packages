Source: haskell-mtl
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Joachim Breitner <nomeata@debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends: debhelper (>= 9),
 haskell-devscripts (>= 0.13),
 cdbs,
 ghc (>= 8),
 ghc-prof,
Build-Depends-Indep: ghc-doc,
Standards-Version: 4.1.4
Homepage: https://github.com/haskell/mtl
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-mtl
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-mtl]

Package: libghc-mtl-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Provides: ${haskell:Provides},
Description: Haskell monad transformer library for GHC${haskell:ShortBlurb}
 MTL is a monad transformer library, inspired by the paper "Functional
 Programming with Overloading and Higher-Order Polymorphism",
 by Mark P Jones (<http://www.cse.ogi.edu/~mpj/>), Advanced School
 of Functional Programming, 1995.
 .
 ${haskell:Blurb}

Package: libghc-mtl-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Provides: ${haskell:Provides},
Description: Haskell monad transformer library for GHC${haskell:ShortBlurb}
 MTL is a monad transformer library, inspired by the paper "Functional
 Programming with Overloading and Higher-Order Polymorphism",
 by Mark P Jones (<http://www.cse.ogi.edu/~mpj/>), Advanced School
 of Functional Programming, 1995.
 .
 ${haskell:Blurb}

Package: libghc-mtl-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Description: Haskell monad transformer library for GHC${haskell:ShortBlurb}
 MTL is a monad transformer library, inspired by the paper "Functional
 Programming with Overloading and Higher-Order Polymorphism",
 by Mark P Jones (<http://www.cse.ogi.edu/~mpj/>), Advanced School
 of Functional Programming, 1995.
 .
 ${haskell:Blurb}
