From: Sergei Trofimovich <slyfox@gentoo.org>
Date: Wed, 18 Jul 2018 22:36:58 +0000 (+0100)
Subject: fix osReserveHeapMemory block alignment
X-Git-Url: https://git.haskell.org/ghc.git/commitdiff_plain/e175aaf6918bb2b497b83618dc4c270a0d231a1c

fix osReserveHeapMemory block alignment

Before the change osReserveHeapMemory() attempted
to allocate chunks of memory via osTryReserveHeapMemory()
not multiple of MBLOCK_SIZE in the following fallback code:

```
    if (at == NULL) {
        *len -= *len / 8;
```

and caused assertion failure:

```
$ make fulltest TEST=T11607 WAY=threaded1
T11607: internal error: ASSERTION FAILED: file rts/posix/OSMem.c, line 457
    (GHC version 8.7.20180716 for riscv64_unknown_linux)

```

The change applies alignment mask before each MBLOCK allocation attempt
and fixes WAY=threaded1 test failures on qemu-riscv64.

Signed-off-by: Sergei Trofimovich <slyfox@gentoo.org>

Test Plan: run 'make fulltest WAY=threaded1'

Reviewers: simonmar, bgamari, erikd

Reviewed By: simonmar

Subscribers: rwbarton, thomie, carter

Differential Revision: https://phabricator.haskell.org/D4982
---

Index: ghc-8.2.2/rts/posix/OSMem.c
===================================================================
--- ghc-8.2.2.orig/rts/posix/OSMem.c
+++ ghc-8.2.2/rts/posix/OSMem.c
@@ -422,6 +422,8 @@
     void *base, *top;
     void *start, *end;
 
+    ASSERT((len & ~MBLOCK_MASK) == len);
+
     /* We try to allocate len + MBLOCK_SIZE,
        because we need memory which is MBLOCK_SIZE aligned,
        and then we discard what we don't need */
@@ -489,6 +491,8 @@
 
     attempt = 0;
     while (1) {
+        *len &= ~MBLOCK_MASK;
+
         if (*len < MBLOCK_SIZE) {
             // Give up if the system won't even give us 16 blocks worth of heap
             barf("osReserveHeapMemory: Failed to allocate heap storage");
