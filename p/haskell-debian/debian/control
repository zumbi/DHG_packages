Source: haskell-debian
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Joachim Breitner <nomeata@debian.org>,
 Dmitry Bogatov <KAction@gnu.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-hunit-dev,
 libghc-hunit-prof,
 libghc-haxml-dev (>= 1:1.20),
 libghc-haxml-prof,
 libghc-listlike-dev (>= 4.3.5),
 libghc-listlike-prof,
 libghc-sha-dev,
 libghc-sha-prof,
 libghc-unixutils-dev (>= 1.52.4),
 libghc-unixutils-prof,
 libghc-bzlib-dev,
 libghc-bzlib-prof,
 libghc-either-dev,
 libghc-either-prof,
 libghc-exceptions-dev,
 libghc-exceptions-prof,
 libghc-haxml-dev,
 libghc-haxml-dev (>= 1:1.20),
 libghc-haxml-prof,
 libghc-hunit-dev,
 libghc-hunit-prof,
 libghc-listlike-dev (>= 4.3.5),
 libghc-listlike-prof,
 libghc-mtl-dev,
 libghc-mtl-prof,
 libghc-network-dev (>= 2.6),
 libghc-network-prof,
 libghc-network-uri-dev (>= 2.6),
 libghc-network-uri-prof,
 libghc-old-locale-dev,
 libghc-old-locale-prof,
 libghc-parsec3-dev,
 libghc-parsec3-dev (<< 4),
 libghc-parsec3-dev (>= 2),
 libghc-parsec3-prof,
 libghc-process-extras-dev (>= 0.2.0),
 libghc-process-extras-prof,
 libghc-puremd5-dev,
 libghc-puremd5-prof,
 libghc-regex-compat-dev,
 libghc-regex-compat-prof,
 libghc-regex-tdfa-dev,
 libghc-regex-tdfa-prof,
 libghc-text-dev,
 libghc-text-prof,
 libghc-unixutils-dev (>= 1.52.4),
 libghc-unixutils-prof,
 libghc-utf8-string-dev,
 libghc-utf8-string-prof,
 libghc-zlib-dev,
 libghc-zlib-prof,
Build-Depends-Indep: ghc-doc,
 libghc-hunit-doc,
 libghc-haxml-doc,
 libghc-listlike-doc,
 libghc-sha-doc,
 libghc-unixutils-doc,
 libghc-bzlib-doc,
 libghc-either-doc,
 libghc-exceptions-doc,
 libghc-haxml-doc,
 libghc-hunit-doc,
 libghc-listlike-doc,
 libghc-mtl-doc,
 libghc-network-doc,
 libghc-network-uri-doc,
 libghc-old-locale-doc,
 libghc-parsec3-doc,
 libghc-process-extras-doc,
 libghc-puremd5-doc,
 libghc-regex-compat-doc,
 libghc-regex-tdfa-doc,
 libghc-text-doc,
 libghc-unixutils-doc,
 libghc-utf8-string-doc,
 libghc-zlib-doc,
Build-Conflicts:
 libghc-parsec2-dev,
Standards-Version: 4.1.4
Homepage: https://github.com/ddssff/debian-haskell
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-debian
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-debian]

Package: libghc-debian-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Haskell library for working with the Debian package system
 This library includes modules covering almost every aspect of the Debian
 packaging system, including low level data types such as version numbers
 and dependency relations, on up to the types necessary for computing and
 installing build dependencies, building source and binary packages,
 and inserting them into a repository.
 .
 ${haskell:Blurb}

Package: libghc-debian-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Profiling library for working with the Debian package system${haskell:ShortBlurb}
 This library includes modules covering almost every aspect of the Debian
 packaging system, including low level data types such as version numbers
 and dependency relations, on up to the types necessary for computing and
 installing build dependencies, building source and binary packages,
 and inserting them into a repository.
 .
 ${haskell:Blurb}

Package: libghc-debian-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Documentation for Debian package system library${haskell:ShortBlurb}
 This library includes modules covering almost every aspect of the Debian
 packaging system, including low level data types such as version numbers
 and dependency relations, on up to the types necessary for computing and
 installing build dependencies, building source and binary packages,
 and inserting them into a repository.
 .
 ${haskell:Blurb}

Package: haskell-debian-utils
Architecture: any
Section: devel
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 apt-file,
Description: Various helpers to work with Debian packages${haskell:ShortBlurb}
 This package contains tools shipped with the Haskell library “debian”:
 .
   * fakechanges:
     Sometimes you have the .debs, .dsc, .tar.gz, .diff.gz, etc from a package
     build, but not the .changes file. This package lets you create a fake
     .changes file in case you need one.
 .
   * debian-report:
     Analyze Debian repositories and generate reports about their contents and
     relations. For example, a list of all packages in a distribution that are
     trumped by another distribution.
 .
   * apt-get-build-depends:
     Tool which will parse the Build-Depends{-Indep} lines from debian/control
     and apt-get install the required packages
