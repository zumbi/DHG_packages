haskell-map-syntax (0.3-2) UNRELEASED; urgency=medium

  * Set Rules-Requires-Root to no.

 -- Clint Adams <clint@debian.org>  Sun, 06 May 2018 22:10:05 -0400

haskell-map-syntax (0.3-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Thu, 26 Apr 2018 17:00:59 -0400

haskell-map-syntax (0.2.0.2-4) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:04:56 -0400

haskell-map-syntax (0.2.0.2-3) unstable; urgency=medium

  * Remove double hspec dependency.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 11 Jul 2017 15:03:17 +0200

haskell-map-syntax (0.2.0.2-2) unstable; urgency=medium

  * debian/patches/531b14ec3ec33d9b9f8535753bbd6b646d956eb6.patch:
    - bump hspec bounds

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 11 Jul 2017 14:35:40 +0200

haskell-map-syntax (0.2.0.2-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Thu, 06 Jul 2017 17:24:18 -0400

haskell-map-syntax (0.2.0.1-3) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:34:49 -0400

haskell-map-syntax (0.2.0.1-2) experimental; urgency=medium

  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Sun, 16 Oct 2016 20:06:51 -0400

haskell-map-syntax (0.2.0.1-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Sean Whitton ]
  * New upstream release.
    - Drop build-dependencies:
      - libghc-test-framework
      - libghc-test-framework-hunit
      - libghc-test-framework-quickcheck2
    - New build-dependency:
      - libghc-hspec-dev

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 13 Aug 2016 12:21:32 -0700

haskell-map-syntax (0.2-3) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:45 -0500

haskell-map-syntax (0.2-2) experimental; urgency=medium

  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:28:08 +0200

haskell-map-syntax (0.2-1) unstable; urgency=low

  [ Debian Haskell Group ]
  * Initial release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 29 Jun 2015 14:07:43 +0200
