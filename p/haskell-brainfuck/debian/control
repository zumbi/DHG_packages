Source: haskell-brainfuck
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-mtl-dev,
 libghc-mtl-prof,
Build-Depends-Indep: ghc-doc, libghc-mtl-doc
Standards-Version: 4.1.4
Homepage: http://hackage.haskell.org/package/brainfuck
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-brainfuck
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-brainfuck]

Package: libghc-brainfuck-dev
Architecture: any
Depends: ${haskell:Depends}, ${misc:Depends}, ${shlibs:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Provides: ${haskell:Provides}
Description: Brainfuck interpreter library${haskell:ShortBlurb}
 It is an interpreter for the Brainfuck language, written in the
 pure, lazy, functional language Haskell.
 .
 ${haskell:Blurb}

Package: libghc-brainfuck-prof
Architecture: any
Depends: ${haskell:Depends}, ${misc:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Provides: ${haskell:Provides}
Description: Brainfuck interpreter library${haskell:ShortBlurb}
 It is an interpreter for the Brainfuck language, written in the
 pure, lazy, functional language Haskell.
 .
 ${haskell:Blurb}

Package: libghc-brainfuck-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends}, ${misc:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Description: Brainfuck interpreter library${haskell:ShortBlurb}
 It is an interpreter for the Brainfuck language, written in the
 pure, lazy, functional language Haskell.
 .
 ${haskell:Blurb}

Package: hsbrainfuck
Architecture: any
Depends: ${haskell:Depends}, ${misc:Depends}, ${shlibs:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Provides: ${haskell:Provides}
Description: interpreter for the brainfuck programming language
 This package provides an interpreter for the Brainfuck programming
 language, written in the pure, lazy, functional language Haskell.
