haskell-edison-core (1.3.2.1-2) UNRELEASED; urgency=medium

  * Set Rules-Requires-Root to no.

 -- Clint Adams <clint@debian.org>  Sun, 06 May 2018 22:09:37 -0400

haskell-edison-core (1.3.2.1-1) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.
  * New upstream release

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 15:52:34 -0400

haskell-edison-core (1.3.1.1-3) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:33:31 -0400

haskell-edison-core (1.3.1.1-2) experimental; urgency=medium

  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Sun, 16 Oct 2016 14:06:31 -0400

haskell-edison-core (1.3.1.1-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)
  * Convert `debian/copyright' to dep5 format

  [ Joachim Breitner ]
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sun, 29 May 2016 12:03:50 +0200

haskell-edison-core (1.3-1) unstable; urgency=medium

  * Add Uploaders field, which I accidentally dropped
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sat, 12 Dec 2015 17:35:23 +0100

haskell-edison-core (1.2.2.1-4) experimental; urgency=medium

  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:27:32 +0200

haskell-edison-core (1.2.2.1-3) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:49:24 +0200

haskell-edison-core (1.2.2.1-2) experimental; urgency=medium

  * Depend on haskell-devscripts 0.9, found in experimental

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 17:10:22 +0100

haskell-edison-core (1.2.2.1-1) unstable; urgency=low

  * Adjust watch file to new hackage layout
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 04 Aug 2014 23:00:03 +0200

haskell-edison-core (1.2.2-2) unstable; urgency=low

  * Enable compat level 9

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:50:31 +0200

haskell-edison-core (1.2.2-1) experimental; urgency=low

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Wed, 23 Jan 2013 09:45:35 +0100

haskell-edison-core (1.2.1.3-10) experimental; urgency=low

  * Depend on haskell-devscripts 0.8.13 to ensure this packages is built
    against experimental
  * Bump standards version, no change

 -- Joachim Breitner <nomeata@debian.org>  Thu, 18 Oct 2012 10:25:18 +0200

haskell-edison-core (1.2.1.3-9) unstable; urgency=low

  * Use standard docs directory, avoids upgrade problems.

 -- Joachim Breitner <nomeata@debian.org>  Sat, 19 May 2012 22:43:35 +0200

haskell-edison-core (1.2.1.3-8) unstable; urgency=low

  * Fix -doc dependency.
  * Bump to Standards-Version 3.9.3.

 -- Clint Adams <clint@debian.org>  Sat, 25 Feb 2012 11:52:18 -0500

haskell-edison-core (1.2.1.3-7) unstable; urgency=low

  * Add Replaces/Conflicts on libghc6-*-doc package (Closes: #656431)

 -- Joachim Breitner <nomeata@debian.org>  Sat, 21 Jan 2012 16:59:58 +0100

haskell-edison-core (1.2.1.3-6) unstable; urgency=low

  [ Marco Silva ]
  * Use ghc instead of ghc6

  [ Iain Lane ]
  * Standards-Version → 3.9.2, no changes required

 -- Iain Lane <laney@debian.org>  Wed, 25 May 2011 11:55:32 +0100

haskell-edison-core (1.2.1.3-5) unstable; urgency=low

  * control: Use versioned Replaces: and Conflicts:

 -- Marco Túlio Gontijo e Silva <marcot@debian.org>  Wed, 05 May 2010 18:01:39 -0300

haskell-edison-core (1.2.1.3-4) unstable; urgency=low

  * debian/source/format: Use 3.0 (quilt).
  * debian/control: Rename -doc package.
  * debian/control: Uploaders: create.

 -- Marco Túlio Gontijo e Silva <marcot@debian.org>  Tue, 16 Mar 2010 11:00:39 -0300

haskell-edison-core (1.2.1.3-3) unstable; urgency=low

  [ Marco Túlio Gontijo e Silva ]
  * debian/control: Change Priority: to extra.
  * debian/watch: Use format that works for --download-current-version.
  * debian/watch: Add .tar.gz to downloaded filename.
  * debian/watch: Include package name in downloaded .tar.gz.
  * debian/watch: Remove spaces, since they're not allowed by uscan.
  * debian/control: Add field Provides: ${haskell:Provides} to -dev and
    -prof packages.
  * debian/control: Use Vcs-Browser: field.
  * debian/control: Use comma in the beginning of the line.
  * debian/control: Remove dependency in hscolour, since it's now a
    dependency of haskell-devscripts.
  * debian/control: Remove haddock from Build-Depends:, since it's now a
    Depends: of haskell-devscripts.
  * debian/control: Bump Standards-Version: to 3.8.4, no changes needed.

  [ Joachim Breitner ]
  * Bump haskell-devscripts dependency to 0.7
  * Remove hugs package

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Feb 2010 18:48:18 +0100

haskell-edison-core (1.2.1.3-2) unstable; urgency=low

  [ Iain Lane ]
  * Add hscolour to Build-Depends-Indep; required for doc build

  [ Joachim Breitner ]
  * Undo B-D/B-D-I split

 -- Joachim Breitner <nomeata@debian.org>  Sun, 16 Aug 2009 18:00:49 +0200

haskell-edison-core (1.2.1.3-1) unstable; urgency=low

  * Adopt haskell-edison for the Debian Haskell Group. Thanks to Arjan for his
    previous maintenance.
  * Edison has been split into multiple source packages.
  * Do packaging from scratch with a standard haskell-devscripts approach.
     + Adds -prof packages.
  * New upstream release, closes: #534038.

 -- Joachim Breitner <nomeata@debian.org>  Fri, 17 Jul 2009 11:32:42 +0200

haskell-edison (1.2.1-9) unstable; urgency=low

  * Fix lintian warning "doc-base-unknown-section haskell-edison-api:5
    Apps/Programming"

 -- Arjan Oosting <arjan@debian.org>  Mon, 17 Mar 2008 00:12:25 +0100

haskell-edison (1.2.1-8) unstable; urgency=low

  * Really bump the build dependency this time around.

 -- Arjan Oosting <arjan@debian.org>  Sun, 13 Jan 2008 18:09:45 +0100

haskell-edison (1.2.1-7) unstable; urgency=low

  * debian/control:
    - Bump build dependency on GHC to >= 6.8.2  because the package now
      needs a Cabal version >= 1.2.

 -- Arjan Oosting <arjan@debian.org>  Sun, 13 Jan 2008 16:44:51 +0100

haskell-edison (1.2.1-6) unstable; urgency=low

  * Update for GHC 6.8.2:
    - debian/patches/01_update-cabal-file.dpatch:
      + Add array and containers to the Build-Depends which are libraries
        which are split of the base library.
      + Add FlexibleInstances, TypeSynonymInstances PatternSignatures and
        FlexibleContexts as needed Extensions.
      + Add -fglashow-exts to the Ghc-Options so GHC will parse
        unsafeCoerce# properly.
    - debian/patches/20_add-haddock-file.dpatch: Updated.
    - debian/rules:
      + The location of the package specification file has changes, so
        adjust the debian/rules file for that.
      + Add a link to the COPYRIGHT file in de edison-api and edison-core
        directories as the new Cabal version tries to install it and fails
        if it is missing.
      + Remove these links on clean.
      + Remove the installed COPYRIGHT files. No need to duplicate this
        information in the packages.
  * debian/control:
    - Replace Xs-Vcs-* fields with new official Vcs-* fields.
    - Bump Standards-Version to 3.7.3. No changes needed.
    - Drop dctrl-tools from the build dependencies as the script using it
      is removed.
    - Add haskell-devscripts (>= 0.6.0) to the build dependencies because
      we user dh_haskell_depends.
    - Change the sections of the libghc-*-dev packages to libdevel to fix
      lintian warning "dev-package-should-be-section-libdevel".
  * debian/rules:
    - Replace call to debian/mk-haskell-depends script with call to
      dh_haskell_depends from haskell-devscripts.

 -- Arjan Oosting <arjan@debian.org>  Sun, 13 Jan 2008 01:56:06 +0100

haskell-edison (1.2.1-5) unstable; urgency=high

  * Set urgency to high as this upload fixes a RC critical bug.
  * debian/control:
    - Use the new Homepage field.
  * debian/rules:
    - Make the determination of package names and versions more robust.
    - gcc 4.2.0 and higher cause FTFBS if we enable --split-objs during
     the compilation of haskell-edison. As a workaround we disable
     --split-objs if the gcc version >= 4.2.0 (Closes: #445754)

 -- Arjan Oosting <arjan@debian.org>  Mon, 08 Oct 2007 23:39:49 +0200

haskell-edison (1.2.1-4) unstable; urgency=low

  * debian/control:
    - Update XS-Vcs-* fields as the packages has moved to the pkg-haskell
      subversion archive.
    - Add libghc6-mtl-prof and libghc6-quickcheck-prof to Build-Depends as
      libghc6-mtl-dev and libghc6-quickcheck-dev have split of their
      profiling libraries into seperate packages.
  * debian/{ghc6.in,rules}:
    - Add handling of noopt in DEB_BUILD_OPTIONS enviroment variable.
  * debian/{mk-haskell-depends,rules}:
    - Small cleanups.
  * Generate links to other API documentation packages:
    - debian/control:
      + Add libghc6-base-doc, libghc6-haskell98-doc, libghc6-mtl-doc and
        libghc6-quickcheck-doc to the Build-Depends-Indep so haddock can
        link to the names in other packages.
      + Add libghc6-base-doc, libghc6-haskell98-doc, libghc6-mtl-doc and
        libghc6-quickcheck-doc to the Recommends of haskell-edison-doc.
    - debian/patches/20_add-haddock-file.dpatch:
      + Add the appropriate --read-interface and --use-package options so
        haddock will link to names in other packages.

 -- Arjan Oosting <arjan@debian.org>  Mon, 28 May 2007 04:12:19 +0200

haskell-edison (1.2.1-3) unstable; urgency=low

  * debian/control:
    - Update Build-Depends-Indep and Depends for the modular packaging of
      Hugs version 98.200609.21.
    - Wrap Build-Depends, Build-Depends-Indep and Depends lines.

 -- Arjan Oosting <arjan@debian.org>  Tue, 17 Apr 2007 19:30:22 +0200

haskell-edison (1.2.1-2) unstable; urgency=low

  * debian/control:
    - Add XS-Vcs-Svn and XS-Vcs-Browser fields.
  * debian/rules: enclose paths in double quotes because directories can
    contain spaces.

 -- Arjan Oosting <arjan@debian.org>  Thu, 12 Apr 2007 10:13:13 +0200

haskell-edison (1.2.1-1) experimental; urgency=low

  * New upstream release:
    - A new sequence implementation based on finger trees (similar to
      Data.Sequence in the base libs).
    - Documenation fixes dealing with the licence (the docs previously and
      incorrectly claimed Edison was under the BSD licence, when it is in
      fact the MIT license).
  * debian/control: add cpphs which is needed to build the documentation
    to Build-Depends-Indep.
  * debian/mk-haskell-depends: update script to take installed package
    configuration files as arguments.

 -- Arjan Oosting <arjan@debian.org>  Fri, 22 Dec 2006 09:43:14 +0100

haskell-edison (1.2.0.1-5) unstable; urgency=low

  * debian/rules:
    - Only call ./Setup configure with -enable-split-objs on i386 and
      amd64 as ghc -split-objs only works on those architectures.

 -- Arjan Oosting <arjan@debian.org>  Mon, 13 Nov 2006 01:42:49 +0100

haskell-edison (1.2.0.1-4) unstable; urgency=low

  * debian/control:
    - Bump Build-Depends on ghc6 as Edison needs a new Cabal version.
    - Add ghc6-prof to the Build-Depends.
    - Let libghc6-edison-api-dev and libghc6-edison-core-dev provide
      libghc6-edison-api-prof and libghc6-edison-core-prof.
  * debian/{ghc6.in,ghc-pkg6.in}:
    - Exit on errors and show the commands executed.
  * debian/patches/20_add-haddock-file: generate haddock interface file
    when generating API documentation.
  * debian/rules:
    - Call ./Setup configure with --enable-split-objs and
      --enable-libary-profiling when compiling with GHC.
    - Let debian/mk-haskell-depends ignore EdisonAPI-1.2.
    - Correct paths to the API documentation in the installed-pkg-config
      files.
    - Remove empty include dir.
    - Do not compress .haddock files.

 -- Arjan Oosting <arjan@debian.org>  Sun, 12 Nov 2006 19:36:05 +0100

haskell-edison (1.2.0.1-3) unstable; urgency=low

  * Stop using update-haskell-control and $ghc6_* variables, as it is not
    necessary and not using it simplifies the work for porters and for me.
  * debian/copyright:
    - include email from upstream with more information about the exact
      licensing.
  * debian/control.in:
    - add libghc6-mtl-dev and libghc6-quickcheck-dev to Build-Depends.
    - replace ${ghc:Depends} with ${haskell:Depends}
    - update maintainer email address.
  * debian/rules:
    - copy the actual package.conf file in which all installed packages
      are registered.
    - remove debian/ghc6 and debian/ghc-pkg6 on clean.
    - replace debian/mk-ghc6-vars script with debian/mk-haskell-depends
      script which determines all dependencies by using the installed
      package description file used by ghc-pkg.

 -- Arjan Oosting <arjan@debian.org>  Mon, 23 Oct 2006 14:08:19 +0200

haskell-edison (1.2.0.1-2) unstable; urgency=low

  * Add versioned build dependency on dpkg-dev as the package uses
    ${binary:Version} and ${source:Version}. Thanks James Westby
  * debian/prerm.in: replaced faulty packagename Hat with @LIBRARY@
    substitution variable.

 -- Arjan Oosting <arjanoosting@home.nl>  Fri, 22 Sep 2006 03:05:26 +0200

haskell-edison (1.2.0.1-1) unstable; urgency=low

  * Initial release (Closes: #382184)

 -- Arjan Oosting <arjanoosting@home.nl>  Thu, 10 Aug 2006 15:05:29 +0200
