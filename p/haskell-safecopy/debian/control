Source: haskell-safecopy
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Giovanni Mascellani <gio@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-cereal-dev (<< 0.6),
 libghc-cereal-dev (>= 0.5),
 libghc-cereal-prof,
 libghc-old-time-dev (<< 1.2),
 libghc-old-time-prof,
 libghc-text-dev (<< 1.3),
 libghc-text-prof,
 libghc-vector-dev (>= 0.10),
 libghc-vector-dev (<< 0.13),
 libghc-vector-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-cereal-doc,
 libghc-old-time-doc,
 libghc-text-doc,
 libghc-vector-doc,
Standards-Version: 4.1.4
Homepage: https://github.com/acid-state/safecopy
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-safecopy
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-safecopy]

Package: libghc-safecopy-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Haskell serialization library with version control - GHC libraries${haskell:ShortBlurb}
 SafeCopy is an extension to cereal, a Haskell serialization library:
 it adds the ability to define new versions of some data tpe and
 automatically convert variables serialized with an old format to a
 newer one.
 .
 ${haskell:Blurb}

Package: libghc-safecopy-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Haskell serialization library with version control - GHC profiling libraries${haskell:ShortBlurb}
 SafeCopy is an extension to cereal, a Haskell serialization library:
 it adds the ability to define new versions of some data tpe and
 automatically convert variables serialized with an old format to a
 newer one.
 .
 ${haskell:Blurb}

Package: libghc-safecopy-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Haskell serialization library with version control - documentation${haskell:ShortBlurb}
 SafeCopy is an extension to cereal, a Haskell serialization library:
 it adds the ability to define new versions of some data tpe and
 automatically convert variables serialized with an old format to a
 newer one.
 .
 ${haskell:Blurb}
