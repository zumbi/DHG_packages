Source: haskell-gitlib
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-base16-bytestring-dev (>= 0.1.1.5),
 libghc-base16-bytestring-prof,
 libghc-conduit-dev (>= 1.2.8),
 libghc-conduit-prof,
 libghc-exceptions-dev (>= 0.5),
 libghc-exceptions-prof,
 libghc-hashable-dev (>= 1.1.2.5),
 libghc-hashable-prof,
 libghc-mtl-dev (>= 2.1.2),
 libghc-mtl-prof,
 libghc-resourcet-dev (>= 1.1.0),
 libghc-resourcet-prof,
 libghc-semigroups-dev (>= 0.11),
 libghc-semigroups-prof,
 libghc-tagged-dev (>= 0.2.3.1),
 libghc-tagged-prof,
 libghc-text-dev (>= 0.11.2),
 libghc-text-prof,
 libghc-unliftio-dev,
 libghc-unliftio-prof,
 libghc-unliftio-core-dev (>= 0.1.1),
 libghc-unliftio-core-prof,
 libghc-unordered-containers-dev (>= 0.2.3.0),
 libghc-unordered-containers-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-base16-bytestring-doc,
 libghc-conduit-doc,
 libghc-exceptions-doc,
 libghc-hashable-doc,
 libghc-mtl-doc,
 libghc-resourcet-doc,
 libghc-semigroups-doc,
 libghc-tagged-doc,
 libghc-text-doc,
 libghc-unliftio-doc,
 libghc-unliftio-core-doc,
 libghc-unordered-containers-doc,
Standards-Version: 4.1.4
Homepage: https://github.com/jwiegley/gitlib
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-gitlib
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-gitlib]
X-Description: API library for working with Git repositories
 gitlib is a high-level, lazy and conduit-aware set of abstractions
 for programming with Git types. Several different backends are
 available, including one for the libgit2 C library. The aim is both
 type-safety and convenience of use for Haskell users, combined with
 high performance and minimal memory footprint by taking advantage of
 Haskell's laziness and the conduit library's deterministic resource
 cleanup.
 .
 For further information, as well as typical use cases, see the
 Git.Tutorial module.

Package: libghc-gitlib-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-gitlib-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-gitlib-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
