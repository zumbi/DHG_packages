Source: haskell-pcre-light
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Joachim Breitner <nomeata@debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libpcre3-dev,
 pkg-config (>=0.9.0),
Build-Depends-Indep: ghc-doc
Standards-Version: 4.1.4
Homepage: https://github.com/Daniel-Diaz/pcre-light
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-pcre-light
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-pcre-light]

Package: libghc-pcre-light-dev
Architecture: any
Depends: libpcre3-dev, ${haskell:Depends}, ${misc:Depends}, ${shlibs:Depends}
Suggests: ${haskell:Suggests}
Provides: ${haskell:Provides}
Description: Haskell library for Perl 5-compatible regular expressions${haskell:ShortBlurb}
 The PCRE library is a set of functions that implement regular expression
 pattern matching using the same syntax and semantics as Perl 5.  The library
 aims to be light, efficient and portable.
 .
 ${haskell:Blurb}

Package: libghc-pcre-light-prof
Architecture: any
Depends: ${haskell:Depends}, ${misc:Depends}
Provides: ${haskell:Provides}
Description: pcre-light library with profiling enabled${haskell:ShortBlurb}
 The PCRE library is a set of functions that implement regular expression
 pattern matching using the same syntax and semantics as Perl 5.  The library
 aims to be light, efficient and portable.
 .
 ${haskell:Blurb}

Package: libghc-pcre-light-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends}, ${misc:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Conflicts: haskell-pcre-light-doc (<< 0.3.1.1-2)
Provides: haskell-pcre-light-doc
Replaces: haskell-pcre-light-doc (<< 0.3.1.1-2)
Description: library documentation for pcre-light${haskell:ShortBlurb}
 The PCRE library is a set of functions that implement regular expression
 pattern matching using the same syntax and semantics as Perl 5.  The library
 aims to be light, efficient and portable.
 .
 ${haskell:Blurb}
